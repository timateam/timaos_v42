#ifndef _FUEL_H_
#define _FUEL_H_

///////////////////////////////////////////////////////////////

#include "types.h"

///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////

void fuel_reader_init( void );
void fuel_reader_process( void );

uint16_t fuel_reader_get( void );

///////////////////////////////////////////////////////////////

#endif // _FUEL_H_
