#ifndef _GUI_SETUP_H_
#define _GUI_SETUP_H_

///////////////////////////////////////////////////////////////

#include "types.h"

///////////////////////////////////////////////////////////////

enum
{
    SKIN_STATE_IDLE,
    SKIN_STATE_WAIT,
    SKIN_STATE_EDIT,
    SKIN_STATE_DONE
};

enum
{
    EDIT_BUTTON_NONE = 0x00,
    EDIT_BUTTON_B1,
    EDIT_BUTTON_B2,
    EDIT_BUTTON_B3,
    EDIT_BUTTON_B4,
    
    EDIT_SHORT_PRESS = 0x10,
    EDIT_SHORT_RELEASE = 0x20,
    EDIT_LONG_PRESS = 0x30,
    EDIT_LONG_RELEASE = 0x40,

	EDIT_MESSAGE_LIST = 0x80,
	EDIT_MESSAGE_REDISPLAY,
};

enum
{
	SETUP_PARAM_EDIT_NONE,

	SETUP_PARAM_SHOW_BUTTON_B1,
	SETUP_PARAM_SHOW_BUTTON_B2,
	SETUP_PARAM_SHOW_BUTTON_B3,
	SETUP_PARAM_SHOW_BUTTON_B4,

	SETUP_PARAM_SHOW_SELECT_B1,
	SETUP_PARAM_SHOW_SELECT_B2,
	SETUP_PARAM_SHOW_SELECT_B3,
	SETUP_PARAM_SHOW_SELECT_B4,

	SETUP_PARAM_ERASE_B1,
	SETUP_PARAM_ERASE_B2,
	SETUP_PARAM_ERASE_B3,
	SETUP_PARAM_ERASE_B4,

	SETUP_PARAM_DISPLAY1_LINE1,
	SETUP_PARAM_DISPLAY1_LINE2,
	SETUP_PARAM_DISPLAY1_LINE3,
	SETUP_PARAM_DISPLAY1_LINE4,

	SETUP_PARAM_DISPLAY2_LINE1,
	SETUP_PARAM_DISPLAY2_LINE2,
	SETUP_PARAM_DISPLAY2_LINE3,
	SETUP_PARAM_DISPLAY2_LINE4,

	SETUP_PARAM_EDIT_SPEED_UNIT,
	SETUP_PARAM_EDIT_SKIN_INDEX,
	SETUP_PARAM_EDIT_TIME,

	SETUP_PARAM_EDIT_HOUR_10,
	SETUP_PARAM_EDIT_HOUR_1,
	SETUP_PARAM_EDIT_MINUTE_10,
	SETUP_PARAM_EDIT_MINUTE_1,

	SETUP_PARAM_UPDATE_DATETIME,
	SETUP_PARAM_SET_MPH,
	SETUP_PARAM_SET_KPH,
	SETUP_PARAM_INCR_TRIP_INDEX,
	SETUP_PARAM_RESET_TRIP,
    SETUP_PARAM_LOAD_TRIP,
    SETUP_PARAM_SAVE_TRIP,

    /*
	SETUP_PARAM_CONTROL_BATTERY_ICON,
	SETUP_PARAM_CONTROL_OIL_ICON,
	SETUP_PARAM_CONTROL_BRAKE_ICON,
	SETUP_PARAM_CONTROL_HEADLAMP_ICON,
	SETUP_PARAM_CONTROL_FOGLIGHT_ICON,
	SETUP_PARAM_CONTROL_FULLBEAM_ICON,
	SETUP_PARAM_CONTROL_MAINBEAM_ICON,
	SETUP_PARAM_CONTROL_ENGINE_ICON,
	SETUP_PARAM_CONTROL_INDICATOR_ICON,
    SETUP_PARAM_CONTROL_TEMP_WARNING_ICON,

	SETUP_PARAM_SHOW_SPEED,
	SETUP_PARAM_SHOW_RPM,
	SETUP_PARAM_SHOW_FUEL,
	SETUP_PARAM_SHOW_TEMPERATURE,
	SETUP_PARAM_UPDATE_ODOMETER,
*/
};

///////////////////////////////////////////////////////////////

void gui_setup_init( void );
void gui_setup_process( void );

///////////////////////////////////////////////////////////////

#endif // _TONE_H_
