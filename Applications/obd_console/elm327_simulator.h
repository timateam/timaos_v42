#ifndef _ELM327_SIMULATOR_H_
#define _ELM327_SIMULATOR_H_

///////////////////////////////////////////////////////////////

#include "types.h"

///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////

void elm327_simulator_init( void );
void elm327_simulator_process( void );

///////////////////////////////////////////////////////////////

#endif // _ELM327_SIMULATOR_H_
