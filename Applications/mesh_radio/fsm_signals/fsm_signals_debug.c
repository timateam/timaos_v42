#include "types.h"

////////////////////////////////////////////////////////////////////

const char * signal_debug[] =
{
    "",
    "Received_Frame_indication",
    "Transmit_Frame_request",
    "Transmit_Done_indication",
    "Set_Channel_request",
    "Set_Channel_confirm",
    "Unset_Channel_request",
    "Listening_indication",
    "Listen_Done_indication",
    "Listen_Timeout_indication",
    "Debug_Print_indication",
    "Scan_Start_request",
    "Scan_Start_confirm",
    "Scan_Result_indication",
    "Scan_Finish_indication",
};

////////////////////////////////////////////////////////////////////

