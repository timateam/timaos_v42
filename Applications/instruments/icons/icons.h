#ifndef __icons_h__
#define __icons_h__

///////////////////////////////////////////////////////////////

#include "types.h"

///////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////

extern const bitmap_t battery_icon_bitmap;
extern const bitmap_t lane_icon_bitmap;
extern const bitmap_t engine_icon_bitmap;
extern const bitmap_t epc_icon_bitmap;
extern const bitmap_t wash_icon_bitmap;
extern const bitmap_t lamp_icon_bitmap;
extern const bitmap_t full_icon_bitmap;
extern const bitmap_t brake_icon_bitmap;
extern const bitmap_t brake_icon_bitmap;
extern const bitmap_t brake_icon_bitmap;
extern const bitmap_t indicator_left_icon_bitmap;
extern const bitmap_t indicator_right_icon_bitmap;
extern const bitmap_t hazard_icon_bitmap;
extern const bitmap_t fog_icon_bitmap;
extern const bitmap_t temp_icon_bitmap;
extern const bitmap_t battery_icon_bitmap;
extern const bitmap_t oil_icon_bitmap;
extern const bitmap_t temp_blue_icon_bitmap;
extern const bitmap_t fuel_blue_icon_bitmap;
extern const bitmap_t fuel_icon_bitmap;

///////////////////////////////////////////////////////////////

#endif // __icons_h__
/*  */
